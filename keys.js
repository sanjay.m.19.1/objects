function getKeys(usrObj){
    if(!usrObj) return [];
    let keys = [];
    //iterate through every property and store its key in keys array.
    for(let key in usrObj){
        keys.push(key);
    }
    return keys;
}

module.exports = { getKeys };