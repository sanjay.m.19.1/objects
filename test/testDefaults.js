const { getDefaults } = require('../defaults.js');
const { testObject } = require('../objects.js');

let hidden = {secret_identity: 'batman', super_powers: 'very rich'};

console.log(getDefaults(testObject, hidden));
console.log(getDefaults({first: "David" }, {first: "Mark", last: "Beckham"}));