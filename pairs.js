function keyValueArray(inputObj){
    if(!inputObj) return [];
    let keyValue = [];
    for(let key in inputObj){
        keyValue.push([key, inputObj[key]]);
    }
    return keyValue;
}

module.exports = {keyValueArray};