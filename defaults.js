function getDefaults(inputObj, defaultProps){
    if(!inputObj) return {};
    if(!defaultProps) return inputObj;
    for(let key in defaultProps){
        if(!inputObj.hasOwnProperty(key)){
            inputObj[key] = defaultProps[key];
        }
    }
    return inputObj;
}

module.exports = {getDefaults};