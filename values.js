function getValues(inputObj){
    if(!inputObj) return [];
    let valuesArr = [];
    for(let key in inputObj){
        if(typeof(inputObj[key]) === 'object'){
            valuesArr.push(JSON.stringify(inputObj[key]));
        } else {
            valuesArr.push(inputObj[key]);
        }
    }
    return valuesArr;
}

module.exports = {getValues};