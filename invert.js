function swapKeyValue(inputObj){
    if(!inputObj) return {};
    let swappedObj = {};
    let temp;
    for(let key in inputObj){
        if(typeof(inputObj[key]) == 'object'){
            swappedObj[JSON.stringify(inputObj[key])] = key;
        } else {
            swappedObj[inputObj[key]] = key;
        }
    }
    return swappedObj;
}

module.exports = {swapKeyValue};