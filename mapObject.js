function objectMap(inputObj, callBackFunction){
    if(!inputObj) return {};
    let outputObj = {};
    for(let key in inputObj){
        outputObj[key] = callBackFunction(inputObj[key]);
    }
    return outputObj;
}

module.exports = {objectMap};